package models

import (
	"fmt"
)

// User struct
type User struct {
	UserID  int    `json:"user_id"`
	Name    string `json:"name"`
	Email   string `json:"email"`
	Company string `json:"company"`
	Phone   string `json:"phone"`
}

// GetUsers fetches the users from the database
func GetUsers() ([]User, string) {
	sql := fmt.Sprintf("SELECT user_id, name, email, company, phone FROM `users`")

	result, err := db.Query(sql)

	if err != nil {
		panic(err.Error())
	}

	defer result.Close()

	// Making the array/slice of User struct
	var users []User

	for result.Next() {
		var user User
		result.Scan(&user.UserID, &user.Name, &user.Email, &user.Company, &user.Phone)
		users = append(users, user)
	}

	if users == nil {
		return users, "There are no users!"
	}

	return users, ""
}

// GetUser fetches the user from the database
func GetUser(userID string) (User, string) {
	sql := fmt.Sprintf("SELECT user_id, name, email, company, phone FROM `users` WHERE `user_id` = %s", userID)

	result, err := db.Query(sql)

	if err != nil {
		panic(err.Error())
	}

	defer result.Close()

	// creating variable of User struct
	var user User
	for result.Next() {
		result.Scan(&user.UserID, &user.Name, &user.Email, &user.Company, &user.Phone)
	}

	if user.UserID == 0 {
		return user, "User ID is incorrect"
	}

	return user, ""
}

// AddUser adds a new user to the database
func AddUser(user User) string {
	// Inserting data into DB
	sql := fmt.Sprintf("INSERT INTO `users` SET name = '%s', email = '%s', company = '%s', phone = '%s'", user.Name, user.Email, user.Company, user.Phone)

	result, err := db.Query(sql)

	if err == nil || result != nil {
		return ""
	}

	return "User could not be added"
}
