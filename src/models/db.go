package models

import (
	"database/sql"
	"fmt"

	// mysql driver
	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

// InitDB initializes the database connection
func InitDB() *sql.DB {
	var err error

	username := "sample_dbase"
	password := "sample_dbase"
	hostname := "db4free.net"
	dbname := "sample_dbase"

	// Open the DB connection
	db, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s)/%s", username, password, hostname, dbname))

	// Check for errors
	if err != nil {
		panic(err.Error())
	} else {
		createTables()
	}

	return db
}

func createTables() {
	db.Query("CREATE TABLE IF NOT EXISTS `users` ( `user_id` INT(11) NOT NULL AUTO_INCREMENT, `name` VARCHAR(150) NOT NULL, `email` VARCHAR(255) NOT NULL, `company` TEXT, `phone` VARCHAR(75), PRIMARY KEY (`user_id`) )")
}
