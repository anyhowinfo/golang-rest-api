package controllers

import (
	"encoding/json"
	"net/http"

	"../models"

	"github.com/gorilla/mux"
)

// Response structure
type Response struct {
	StatusCode int         `json:"status_code"`
	Message    string      `json:"message"`
	Data       interface{} `json:"data"`
}

// GetUsers fetches the users
func GetUsers(w http.ResponseWriter, r *http.Request) {
	// Validating the request method
	if r.Method != "GET" {
		http.Error(w, http.StatusText(500), 500)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode("Only GET request can be performed on this route")
	}

	// Fetching the users data through the model
	users, err := models.GetUsers()

	// Check if there is any error
	if err != "" {
		usersData := Response{
			StatusCode: 404,
			Message:    "There seems to be some error while processing the result!",
			Data:       users,
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(usersData)

		return
	}

	// Using the global "Response" struct for feeding response
	usersData := Response{
		StatusCode: 200,
		Message:    "Users are fetched successfully!",
		Data:       users,
	}

	// Sending the users data in response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(usersData)
}

// GetUser fetches the user data
func GetUser(w http.ResponseWriter, r *http.Request) {
	// Validating the request method
	if r.Method != "GET" {
		http.Error(w, http.StatusText(500), 500)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode("Only GET request can be performed on this route")
	}

	params := mux.Vars(r)

	// Fetching the users data through the model
	user, err := models.GetUser(params["userID"])

	if err != "" {
		userdetails := Response{
			StatusCode: 404,
			Message:    "There seems to be some error while processing the result!",
			Data:       user,
		}

		// Return the error
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(userdetails)
		return
	}

	userdetails := Response{
		StatusCode: 200,
		Message:    "User has been fetched successfully!",
		Data:       user,
	}

	// Sending the user data in response
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(userdetails)
}

// AddUser adds a new user
func AddUser(w http.ResponseWriter, r *http.Request) {
	// Validating the request method
	if r.Method != "POST" {
		http.Error(w, http.StatusText(500), 500)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode("Only POST request can be performed on this route")
	}

	decoder := json.NewDecoder(r.Body)

	var user models.User

	err := decoder.Decode(&user)

	if err != nil {
		panic(err)
	}

	error := models.AddUser(user)
	if error != "" {
		userdetails := Response{
			StatusCode: 404,
			Message:    "The data is not inserted successfully!",
			Data:       "The data is not inserted successfully!",
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(userdetails)

		return
	}

	userdetails := Response{
		StatusCode: 200,
		Message:    "User has been added successfully!",
		Data:       "User has been added successfully!",
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(userdetails)
}
