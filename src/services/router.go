package services

import (
	"net/http"

	"../controllers"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// InitServer provides the routes for the API and then starts the server
func InitServer() {
	router := mux.NewRouter()

	// routes are routing to the controllers
	router.HandleFunc("/api/users", controllers.GetUsers).Methods("GET")
	router.HandleFunc("/api/user/{userID}", controllers.GetUser).Methods("GET")
	router.HandleFunc("/api/user", controllers.AddUser).Methods("POST")

	// Starting server and handling CORS
	port := ":8080"
	http.ListenAndServe(port, handlers.CORS(handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}), handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"}), handlers.AllowedOrigins([]string{"*"}))(router))
}
