package main

import (
	"./models"
	"./services"
)

func main() {
	// Initialize database
	db := models.InitDB()

	defer db.Close()

	services.InitServer()
}
